----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/01/2021 11:15:47 AM
-- Design Name: 
-- Module Name: rom_inverse_8_bits_16depth_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rom_inverse_8_bits_16depth_tb is
end rom_inverse_8_bits_16depth_tb;

architecture stimulus of rom_inverse_8_bits_16depth_tb is
  constant CLK_PERIOD : time := 10ns;

  signal clk  : std_logic;
  signal en   : std_logic;
  signal addr : std_logic_vector(7 downto 0);
  signal data : std_logic_vector(15 downto 0);
begin

  -- Instanciate DUT
  DUT: entity work.rom_inverse_8bits_16depth
  port map (
     clk  => clk,
     en   => en,
     addr => addr,
     data => data
  );

  -- Enable
  en <= '0', '1' after 2*CLK_PERIOD;

  -- Drive clock
  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  -- Iterate through addresses
  test_bench: process is
  variable i : integer := 0;
  begin
    addr <= (others => '0');
    wait for 3*CLK_PERIOD;
    for i in 2**addr'length downto 0 loop
      addr <= std_logic_vector(to_unsigned(i, addr'length));
      wait for CLK_PERIOD;
    end loop;
    wait;
  end process test_bench;
  
  -- Verify that outputs correspond to correct ROM entry

end stimulus;
