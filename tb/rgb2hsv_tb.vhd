----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2021 09:54:30 AM
-- Design Name: 
-- Module Name: rgb2hsv_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rgb2hsv_tb is
end rgb2hsv_tb;

architecture stimulus of rgb2hsv_tb is
  constant CLK_PERIOD : time := 10ns;

  signal red :   STD_LOGIC_VECTOR (7 downto 0); -- unsigned
  signal green : STD_LOGIC_VECTOR (7 downto 0); -- unsigned
  signal blue :  STD_LOGIC_VECTOR (7 downto 0); -- unsigned

  signal red_int :   integer range 0 to 255;
  signal green_int : integer range 0 to 255;
  signal blue_int :  integer range 0 to 255;

  signal hue : STD_LOGIC_VECTOR (8 downto 0); -- unsigned
  signal sat : STD_LOGIC_VECTOR (7 downto 0); -- unsigned
  signal val : STD_LOGIC_VECTOR (7 downto 0); -- unsigned

  signal hsv_valid : std_logic;

  signal clk : STD_LOGIC;
  signal en : STD_LOGIC;

begin

  DUT: entity work.rgb2hsv
    port map(
      red => red,
      green => green,
      blue => blue,
  
      hue => hue,
      sat => sat,
      val => val,

      hsv_valid => hsv_valid,

      clk => clk,
      en => en
    );

  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  red <= std_logic_vector(to_unsigned(red_int, red'length));
  green <= std_logic_vector(to_unsigned(green_int, green'length));
  blue <= std_logic_vector(to_unsigned(blue_int, blue'length));

  test_bench: process is
  begin
    en <= '0';
    wait for 2*CLK_PERIOD;
    en <= '1';

    -- Couleur quelconque:
    --  H 254
    --  S 220
    --  V 254
    red_int <= 35;
    green_int <= 41;
    blue_int <= 254;
    wait for 20*CLK_PERIOD;
    en <= '0';

    wait for CLK_PERIOD;
    en <= '1';
    wait for 10*CLK_PERIOD;

    -- Noir:
    --  H 0
    --  S 0
    --  V 0
    red_int <= 0;
    green_int <= 0;
    blue_int <= 0;
    wait for CLK_PERIOD;

    -- Cyan:
    --  H 256
    --  S 255
    --  V 255
    red_int <= 0;
    green_int <= 255;
    blue_int <= 255;
    wait for CLK_PERIOD;

    -- Jaune:
    --  H 128
    --  S 255
    --  V 255
    red_int <= 255;
    green_int <= 255;
    blue_int <= 0;
    wait for CLK_PERIOD;

    -- Magenta:
    --  H 0
    --  S 255
    --  V 255
    red_int <= 255;
    green_int <= 0;
    blue_int <= 255;
    wait for CLK_PERIOD;

    -- Rouge:
    --  H 64
    --  S 255
    --  V 255
    red_int <= 255;
    green_int <= 0;
    blue_int <= 0;
    wait for CLK_PERIOD;

    -- Vert:
    --  H 192
    --  S 255
    --  V 255
    red_int <= 0;
    green_int <= 255;
    blue_int <= 0;
    wait for CLK_PERIOD;

    -- Bleu:
    --  H 256
    --  S 255
    --  V 255
    red_int <= 0;
    green_int <= 0;
    blue_int <= 255;
    wait for CLK_PERIOD;

    -- Bleu p�le:
    --  H 320
    --  S 0
    --  V 108
    red_int <= 0;
    green_int <= 0;
    blue_int <= 108;
    wait for CLK_PERIOD;
    
    wait;
  end process test_bench;

end stimulus;
