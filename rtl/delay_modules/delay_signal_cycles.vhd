----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/13/2021 08:57:49 PM
-- Design Name: 
-- Module Name: delay_signal - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_signal_cycles is
  generic (
    DELAY_CYCLES : integer := 10
  );
  port (
    input   : in STD_LOGIC;
    delayed : out STD_LOGIC;

    clk : in STD_LOGIC;
    rst : in STD_LOGIC
  );
end delay_signal_cycles;

architecture rtl of delay_signal_cycles is
  signal delay_registers : std_logic_vector (DELAY_CYCLES-1 downto 0);
begin

shift_delay: process (clk, rst) is
begin
  if rising_edge(clk) then
    if (rst = '0') then
      delay_registers <= delay_registers(DELAY_CYCLES-2 downto 0) & input;
    else
      delay_registers <= (others => '0');
    end if;
    delayed <= delay_registers(DELAY_CYCLES-1);
  end if;

end process shift_delay;

end rtl;
