----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/05/2021 01:37:37 PM
-- Design Name: 
-- Module Name: delay_vector - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_vector_cycles is
  generic (
    DELAY_CYCLES : integer := 10; -- 0.2μs
    WIDTH : integer := 8
  );
  port (
    input   : in std_logic_vector(WIDTH-1 downto 0);
    delayed : out std_logic_vector(WIDTH-1 downto 0);

    clk : in STD_LOGIC;
    rst : in STD_LOGIC
  );
end delay_vector_cycles;

architecture rtl of delay_vector_cycles is
begin

  gen_delay_units: for i in WIDTH-1 downto 0 generate
  begin
    delay_unit : entity work.delay_signal_cycles
      generic map (
        DELAY_CYCLES => DELAY_CYCLES
      )
      port map (
        input   => input(i),
        delayed => delayed(i),
    
        clk => clk,
        rst => rst
      );
  end generate;

end rtl;
