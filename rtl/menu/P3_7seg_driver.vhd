----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/08/2021 04:59:30 PM
-- Design Name: 
-- Module Name: P3_7seg_driver - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity P3_7seg_driver is
  port (
    print_data : in std_logic_vector(31 downto 0);
    
    --  +---+---+---+---+---+---+---+----+
    --  | A | B | C | D | E | F | G | DP |
    --  +---+---+---+---+---+---+---+----+
    --  | 7 | 6 | 5 | 4 | 3 | 2 | 1 |  0 |
    --  +---+---+---+---+---+---+---+----+
    segments : out std_logic_vector(7 downto 0);
    
    -- Displays: MSB: leftmost; lsb: rightmost
    ds_en : out std_logic_vector(3 downto 0);
    
    en : in std_logic;
    clk : in std_logic -- 100MHz
  );
end P3_7seg_driver;

architecture rtl of P3_7seg_driver is
  signal addr : unsigned(1 downto 0);
  signal clk_7seg : std_logic;
begin

  clk_divider: entity work.P3_7seg_clk(rtl)
    port map (
      clk_in => clk,
      en => en,
      clk_7seg => clk_7seg
    );
  
  -- Cycle through each display
  -- '1' => display powered
  ds_en <= "0000" when en = '0'
    else "0001" when addr = "00"
    else "0010" when addr = "01"
    else "0100" when addr = "10"
    else "1000";

  -- MUX data to the selected display
--  segments <= (others => 0) when en = '0'
--    else print_data(31 downto 24) when "00"
--    else print_data(23 downto 16) when "01"
--    else print_data(15 downto 8)  when "10"
--    else print_data(7 downto 0);
    
  with addr select segments <=
    print_data(31 downto 24) when "00",
    print_data(23 downto 16) when "01",
    print_data(15 downto 8)  when "10",
    print_data(7 downto 0)   when others;

  -- Counter
  registered: process (clk_7seg, en) is
  begin
    if (en = '0') then
      addr <= "00";
    elsif (rising_edge(clk_7seg)) then
      addr <= addr + 1;
    end if;
  end process registered;
  
end rtl;
