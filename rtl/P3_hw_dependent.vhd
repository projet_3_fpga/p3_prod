----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/08/2021 04:09:55 PM
-- Design Name: 
-- Module Name: P3_hw_dependent - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity P3_hw_dependent is
    port (
      btns_hw : in STD_LOGIC_VECTOR (3 downto 0);
      seven_segment_hw : out STD_LOGIC_VECTOR (7 downto 0);
      seven_seg_ds_en_hw : out STD_LOGIC_VECTOR (3 downto 0);
      red_s : out STD_LOGIC;
      green_s : out STD_LOGIC;
      blue_s : out STD_LOGIC;
      clock_s : out STD_LOGIC;
      rst_btn : in STD_LOGIC;
      crystal_clk : in STD_LOGIC
    );
end P3_hw_dependent;

architecture structural of P3_hw_dependent is
  -- Signals
  -- Clocking wizard
  signal dvi_clk    : std_logic;
  signal dvi_clk_n  : std_logic;
  signal pclk       : std_logic;
  signal clk_100MHz : std_logic;
  signal clk_locked : std_logic;
  
  component clk_wiz_0
    port (
      -- Clock out ports  
      signal dvi_clk    : out std_logic;
      signal dvi_clk_n  : out std_logic;
      signal pclk       : out std_logic;
      signal clk_100MHz : out std_logic;
      signal locked     : out std_logic;
 
      -- Clock in ports
      signal clk_in : in std_logic;
      signal reset : in std_logic
    );
  end component;

  -- Hardware BSP
  signal btns            : std_logic_vector(3 downto 0);
  signal seven_segment   : std_logic_vector(7 downto 0);
  signal seven_seg_ds_en : std_logic_vector(3 downto 0);

begin
  -- GPIO HW logic adjustment
  mimas_a7v3_hw_bsp_inst: entity work.mimas_a7v3_hw_bsp
    port map (
      btns_hw            => btns_hw,
      btns               => btns,
      seven_segment_hw   => seven_segment_hw,
      seven_segment      => seven_segment,
      seven_seg_ds_en_hw => seven_seg_ds_en_hw,
      seven_seg_ds_en    => seven_seg_ds_en,
      
      clk_100MHz => clk_100MHz
    );

  -- Instanciate clock wizard
  inst_mmcm : clk_wiz_0
    port map ( 
      -- Clock out ports  
      dvi_clk     => dvi_clk,
      dvi_clk_n   => dvi_clk_n,
      pclk        => pclk,
      clk_100MHz  => clk_100MHz,
      locked      => clk_locked,
      
      -- Clock in ports
      clk_in => crystal_clk,
      reset => rst_btn
    );
  
  -- HW-independent block
  P3_integration_no_camera_inst: entity work.P3_integration_no_camera
    port map (
      -- Buttons; debounced, non-inverted logic (1 = pressed)
      menu_btns => btns,
  
      -- 7-segments, non-inverted logic (1 = lit, enabled)
      seven_segment   => seven_segment,
      seven_seg_ds_en => seven_seg_ds_en,

      -- DVI diff pairs
      red_s   => red_s,
      green_s => green_s,
      blue_s  => blue_s,
      clock_s => clock_s,
      
      -- OV7670 camera
      -- none
  
      -- Clocking
      dvi_clk    => dvi_clk,
      dvi_clk_n  => dvi_clk_n,
      pclk       => pclk,
      clk_100MHz => clk_100MHz,
      clk_locked => clk_locked
    );

end structural;
