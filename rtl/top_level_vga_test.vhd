----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/01/2021 06:06:02 PM
-- Design Name: 
-- Module Name: top_level_vga_test - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: Leaf cell instanciation only
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_level_vga_test is
  port (
    enable        : out std_logic_vector(3 downto 0); -- écran gauche: enable(0)
    seven_segment : out std_logic_vector(7 downto 0); -- Ordre: 7 downto 0 => EDC<DP>BAFG
    sw_in         : in std_logic_vector(3 downto 0);  -- Ordre: 3 downto 0 => Up Down Right Left

    hdmi_tx_p      : out  STD_LOGIC_VECTOR(2 downto 0);
    hdmi_tx_n      : out  STD_LOGIC_VECTOR(2 downto 0);
    hdmi_tx_clk_p  : out  std_logic;
    hdmi_tx_clk_n  : out  std_logic;
    
    CLK1 : in std_logic;
    RESET : in std_logic
  );
end top_level_vga_test;

architecture rtl of top_level_vga_test is

  -- DVI diff pairs
  signal red_s   : std_logic;
  signal green_s : std_logic;
  signal blue_s  : std_logic;
  signal clock_s : std_logic;

  signal crystal_clk : std_logic;
begin

  -- Distribution d'horloge
  BUFG_inst : BUFG
    port map (
       O => crystal_clk,
       I => CLK1
    );

  inst_P3_hw_dependent: entity work.P3_hw_dependent
    port map (
      btns_hw            => sw_in,
      seven_segment_hw   => seven_segment,
      seven_seg_ds_en_hw => enable,
      red_s              => red_s,
      green_s            => green_s,
      blue_s             => blue_s,
      clock_s            => clock_s,
      rst_btn            => RESET,
      crystal_clk        => crystal_clk
    );

OBUFDS_red    : OBUFDS port map ( O  => hdmi_tx_p(2), OB => hdmi_tx_n(2), I  => red_s   );
OBUFDS_green  : OBUFDS port map ( O  => hdmi_tx_p(1), OB => hdmi_tx_n(1), I  => green_s );
OBUFDS_blue   : OBUFDS port map ( O  => hdmi_tx_p(0), OB => hdmi_tx_n(0), I  => blue_s  );
OBUFDS_clock  : OBUFDS port map ( O  => hdmi_tx_clk_p, OB => hdmi_tx_clk_n, I  => clock_s );

end rtl;
