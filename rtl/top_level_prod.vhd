----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/01/2021 06:06:02 PM
-- Design Name: 
-- Module Name: top_level - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_level_prod is
  port (
    hdmi_tx_p      : out  STD_LOGIC_VECTOR(2 downto 0);
    hdmi_tx_n      : out  STD_LOGIC_VECTOR(2 downto 0);
    hdmi_tx_clk_p  : out  std_logic;
    hdmi_tx_clk_n  : out  std_logic;
    
    CLK1 : in std_logic;
    RESET : in std_logic
  );
end top_level_prod;

architecture rtl of top_level_prod is
  -- VGA outputs
  signal red        : STD_LOGIC_VECTOR (7 downto 0);
  signal green      : STD_LOGIC_VECTOR (7 downto 0);
  signal blue       : STD_LOGIC_VECTOR (7 downto 0);
  signal hsync      : STD_LOGIC;
  signal vsync      : STD_LOGIC;
  signal blank      : STD_LOGIC;

  -- Adjustment registers
  signal hue_add : STD_LOGIC_VECTOR (9 downto 0); -- signed
  signal sat_add : STD_LOGIC_VECTOR (8 downto 0); -- signed
  signal val_add : STD_LOGIC_VECTOR (8 downto 0); -- signed

  -- HSV2RGB outputs
  signal red_adjusted   : STD_LOGIC_VECTOR (7 downto 0);
  signal green_adjusted : STD_LOGIC_VECTOR (7 downto 0);
  signal blue_adjusted  : STD_LOGIC_VECTOR (7 downto 0);

  -- Delay outputs
  signal hSync_delayed : STD_LOGIC;
  signal vSync_delayed : STD_LOGIC;
  signal blank_delayed : STD_LOGIC;

  -- clk, rst
  signal crystal_clk : STD_LOGIC;
  signal dvi_clk : STD_LOGIC;
  signal dvi_clk_n : STD_LOGIC;
  signal clk : STD_LOGIC;
  signal clk_locked : STD_LOGIC;
  signal rst : STD_LOGIC;
  signal en  : STD_LOGIC;
  
  -- DVI diff pairs
  signal red_s   : std_logic;
  signal green_s : std_logic;
  signal blue_s  : std_logic;
  signal clock_s : std_logic;
  
  component clk_wiz_0
    port (
      -- Clock out ports  
      signal clk     : out std_logic;
      signal clk_n   : out std_logic;
      signal pclk    : out std_logic;
      signal locked  : out std_logic;
 
      -- Clock in ports
      signal clk_in : in std_logic;
      signal reset : in std_logic
    );
  end component;
begin
  -- Reset
  en <= clk_locked;
  rst <= not en;

  -- Distribution d'horloge
  BUFG_inst : BUFG
    port map (
       O => crystal_clk,
       I => CLK1
    );

  -- Instanciate clock wizard
  inst_mmcm : clk_wiz_0
     port map ( 
     -- Clock out ports  
     clk => dvi_clk,
     clk_n => dvi_clk_n,
     pclk => clk,
     locked => clk_locked,

     -- Clock in ports
     clk_in => crystal_clk,
     reset => RESET
   );

  -- Instanciate VGA test generator
  inst_vga: entity work.vga
  port map (
    pixelClock => clk,
    Red        => red,
    Green      => green,
    Blue       => blue,
    hSync      => hsync,
    vSync      => vsync,
    blank      => blank
  );

  -- Adjustment registers
  hue_add <= std_logic_vector(to_signed(64 , hue_add'length));
  sat_add <= std_logic_vector(to_signed(0 , sat_add'length));
  val_add <= std_logic_vector(to_signed(-150 , val_add'length));

  -- Instanciate adjust unit
  inst_video_adjust_unit: entity work.video_adjust_unit
    port map (
      red_i   => red,
      green_i => green,
      blue_i  => blue,
      hsync_i => hsync,
      vsync_i => vsync,
      blank_i => blank,

      hue_add => hue_add,
      sat_add => sat_add,
      val_add => val_add,

      red_o   => red_adjusted,
      green_o => green_adjusted,
      blue_o  => blue_adjusted,
      hsync_o => hsync_delayed,
      vsync_o => vsync_delayed,
      blank_o => blank_delayed,
    
      clk => clk,
      en  => en
    );

  -- Instanciate DVI out
  inst_dvi: entity work.dvid
    port map (
      clk       => dvi_clk,
      clk_n     => dvi_clk_n,
      clk_pixel => clk,

      red_p     => red_adjusted,
      green_p   => green_adjusted,
      blue_p    => blue_adjusted,
      blank     => blank_delayed,
      hsync     => hsync_delayed,
      vsync     => vsync_delayed,

      red_s     => red_s,
      green_s   => green_s,
      blue_s    => blue_s,
      clock_s   => clock_s
    );

OBUFDS_blue  : OBUFDS port map ( O  => hdmi_tx_p(0), OB => hdmi_tx_n(0), I  => blue_s  );
OBUFDS_red   : OBUFDS port map ( O  => hdmi_tx_p(1), OB => hdmi_tx_n(1), I  => green_s );
OBUFDS_green : OBUFDS port map ( O  => hdmi_tx_p(2), OB => hdmi_tx_n(2), I  => red_s   );
OBUFDS_clock : OBUFDS port map ( O  => hdmi_tx_clk_p, OB => hdmi_tx_clk_n, I  => clock_s );

end rtl;
