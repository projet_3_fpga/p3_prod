----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07/05/2021 09:53:00 AM
-- Design Name:
-- Module Name: rgb2hsv - rtl
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rgb2hsv is
    port (
      red :   in STD_LOGIC_VECTOR (7 downto 0); -- unsigned
      green : in STD_LOGIC_VECTOR (7 downto 0); -- unsigned
      blue :  in STD_LOGIC_VECTOR (7 downto 0); -- unsigned

      hue : out STD_LOGIC_VECTOR (8 downto 0); -- unsigned
      sat : out STD_LOGIC_VECTOR (7 downto 0); -- unsigned
      val : out STD_LOGIC_VECTOR (7 downto 0); -- unsigned

      hsv_valid : out std_logic; -- outputs on hue, sat, val are valid pixels.
  
      clk : in STD_LOGIC;
      en : in STD_LOGIC
    );
end rgb2hsv;

architecture rtl of rgb2hsv is

  -- Déclaration des LUT
  component rom_inverse_8bits_16depth is
    port (
      clk  : in  std_logic;
      en   : in  std_logic;
      addr : in  std_logic_vector(7 downto 0);
      data : out std_logic_vector(15 downto 0)
    );
  end component;

  type registered_t is record
    -- Cycle 1
    r_minus_g : std_logic_vector(8 downto 0);
    g_minus_b : std_logic_vector(8 downto 0);
    b_minus_r : std_logic_vector(8 downto 0);

    red_buf_1 : std_logic_vector(7 downto 0);
    green_buf_1 : std_logic_vector(7 downto 0);
    blue_buf_1 : std_logic_vector(7 downto 0);

    -- Cycle 2
    min_comp : std_logic_vector(7 downto 0);
    max_comp : std_logic_vector(7 downto 0);
    relevant_subtraction: std_logic_vector(8 downto 0);
    offset : std_logic_vector(8 downto 0);

    -- Cycle 3
    max_minus_min : std_logic_vector(7 downto 0);
    relevant_subtraction_buf_3 : std_logic_vector(8 downto 0);
    max_comp_buf_3 : std_logic_vector(7 downto 0);
    offset_buf_3 : std_logic_vector(8 downto 0);

    -- Cycle 4
    inverse_max_minus_min : std_logic_vector(15 downto 0);
    inverse_max_comp : std_logic_vector(15 downto 0);
    
    relevant_subtraction_buf_4 : std_logic_vector(8 downto 0);
    max_comp_buf_4 : std_logic_vector(7 downto 0);
    offset_buf_4 : std_logic_vector(8 downto 0);
    max_minus_min_buf_4 : std_logic_vector(7 downto 0);

    -- Cycle 5
    hue_no_offset : std_logic_vector(24 downto 0);
    sat_division : std_logic_vector(23 downto 0);
    
    relevant_subtraction_buf_5 : std_logic_vector(8 downto 0);
    max_comp_buf_5 : std_logic_vector(7 downto 0);
    offset_buf_5 : std_logic_vector(8 downto 0);
    max_minus_min_buf_5 : std_logic_vector(7 downto 0);

    -- Cycle 6
    max_comp_buf_6 : std_logic_vector(7 downto 0);
    offset_buf_6 : std_logic_vector(8 downto 0);
    max_minus_min_buf_6 : std_logic_vector(7 downto 0);

    -- Cycle 7
    -- Write directly on output ports
  end record registered_t;

  signal reg : registered_t;

  signal inverter_max_minus_min_o : std_logic_vector(15 downto 0);
  signal inverter_max_comp_o : std_logic_vector(15 downto 0);

begin
      
  -- Pipeline
  -- For loop pour chaque cycle du pipeline
  -- 1: Subtractions
  -- 2: min comp, max comp, relevant subtraction, buffer input colors
  -- 3: min_minus_max, offset, buffer relevant subtr
  -- 4: inversion de maxcomp et min_minus_max
  -- 5: multiplication par l'inverse et shifts
  -- 6: ajout de l'offset, détermination des sorties
  pipeline: process(clk, en)
    -- Cycle 2
    variable r_min : std_logic;
    variable g_min : std_logic;
    variable b_min : std_logic;

    variable r_max : std_logic;
    variable g_max : std_logic;
    variable b_max : std_logic;

    -- Cycle 7
    constant MAX_SAT : unsigned (7 downto 0) := to_unsigned(255, 8);
    variable hue_with_offset : std_logic_vector(14 downto 0);
    variable hue_no_offset_shifted : std_logic_vector(14 downto 0);
    variable sat_temp : std_logic_vector(31 downto 0);

  begin
    if rising_edge(clk) then
      if (en = '1') then
        -- Cycle 1
        reg.r_minus_g <= std_logic_vector(signed('0' & red)   - signed('0' & green));
        reg.g_minus_b <= std_logic_vector(signed('0' & green) - signed('0' & blue));
        reg.b_minus_r <= std_logic_vector(signed('0' & blue)  - signed('0' & red));

        reg.red_buf_1 <= red;
        reg.green_buf_1 <= green;
        reg.blue_buf_1 <= blue;
  
        -- Cycle 2
        r_min := reg.r_minus_g(8) and (not reg.b_minus_r(8));
        g_min := reg.g_minus_b(8) and (not reg.r_minus_g(8));
        b_min := reg.b_minus_r(8) and (not reg.g_minus_b(8));
  
        r_max := (not reg.r_minus_g(8)) and reg.b_minus_r(8);
        g_max := (not reg.g_minus_b(8)) and reg.r_minus_g(8);
        b_max := (not reg.b_minus_r(8)) and reg.g_minus_b(8);
  
        reg.min_comp <= reg.red_buf_1 when r_min = '1'
                else reg.green_buf_1 when g_min = '1'
                else reg.blue_buf_1;
  
        reg.max_comp <= reg.red_buf_1 when r_max = '1'
                else reg.green_buf_1 when g_max = '1'
                else reg.blue_buf_1;
  
        reg.relevant_subtraction <= reg.g_minus_b when r_max = '1'
                            else reg.b_minus_r when g_max = '1'
                            else reg.r_minus_g;
  
        reg.offset <= std_logic_vector(to_unsigned(64, reg.offset'length)) when r_max = '1'
             else std_logic_vector(to_unsigned(128+64, reg.offset'length)) when g_max = '1'
             else std_logic_vector(to_unsigned(256+64, reg.offset'length));
  
        -- Cycle 3
        reg.relevant_subtraction_buf_3 <= reg.relevant_subtraction;
        reg.max_comp_buf_3 <= reg.max_comp;
        reg.offset_buf_3 <= reg.offset;
        reg.max_minus_min <= std_logic_vector(unsigned(reg.max_comp) - unsigned(reg.min_comp));

        -- Cycle 4
        reg.relevant_subtraction_buf_4 <= reg.relevant_subtraction_buf_3;
        
        reg.max_comp_buf_4 <= reg.max_comp_buf_3;
        reg.offset_buf_4 <= reg.offset_buf_3;
        reg.max_minus_min_buf_4 <= reg.max_minus_min;
        
        -- Cycle 5
        reg.relevant_subtraction_buf_5 <= reg.relevant_subtraction_buf_4;
        reg.inverse_max_minus_min <= inverter_max_minus_min_o;
        reg.inverse_max_comp <= inverter_max_comp_o;

        reg.max_comp_buf_5 <= reg.max_comp_buf_4;
        reg.offset_buf_5 <= reg.offset_buf_4;
        reg.max_minus_min_buf_5 <= reg.max_minus_min_buf_4;

        -- Cycle 6
        reg.hue_no_offset <= std_logic_vector(signed(reg.relevant_subtraction_buf_5) * signed(reg.inverse_max_minus_min));
        reg.sat_division <= std_logic_vector(unsigned(reg.max_minus_min_buf_5) * unsigned(reg.inverse_max_comp));

        reg.max_comp_buf_6 <= reg.max_comp_buf_5;
        reg.offset_buf_6 <= reg.offset_buf_5;
        reg.max_minus_min_buf_6 <= reg.max_minus_min_buf_5;

        -- Cycle 7
        hue_no_offset_shifted := reg.hue_no_offset(24 downto 10); -- (24 downto 16) *64 (<< 6)
        hue_with_offset := std_logic_vector(signed(hue_no_offset_shifted) + signed("000000" & reg.offset_buf_6));
        hue <= std_logic_vector( unsigned(hue_with_offset(8 downto 0)) ) when reg.max_minus_min_buf_6 /= x"00"
            else (others => '0'); -- Grayscale colors have no hue

        sat_temp := std_logic_vector(MAX_SAT * unsigned(reg.sat_division));
        sat <= sat_temp(23 downto 16);
        val <= reg.max_comp_buf_6;

      else
        -- Reset synchrone
        reg <= (
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
  
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
  
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
  
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
  
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
  
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0')
        );
        
        hue <= (others => '0');
        sat <= (others => '0');
        val <= (others => '0');
      end if;
    end if;
  end process pipeline;

  inverter_max_minus_min: rom_inverse_8bits_16depth
    port map (
      clk => clk,
      en => '1', -- FIXME: Fancy space heater. ROM devrait pas être allumée en permanance
      addr => reg.max_minus_min,
      data => inverter_max_minus_min_o
    );
  
  inverter_max_comp: rom_inverse_8bits_16depth
    port map (
      clk => clk,
      en => '1', -- FIXME: Fancy space heater. ROM devrait pas être allumée en permanance
      addr => reg.max_comp_buf_3,
      data => inverter_max_comp_o
    );
  
  data_valid_counter: process(clk, en)
    variable count : integer range 0 to 7;
  begin
    if rising_edge(clk) then
      if en = '1' then
        -- Increment counter until 7, then stop
        if (count < 7) then
          count := count + 1;
        else
          count := count;
        end if;
      else
        -- Reset counter
        count := 0;
      end if;
    end if;
    
    hsv_valid <= '1' when count = 7 else '0';
  end process data_valid_counter;

end rtl;
