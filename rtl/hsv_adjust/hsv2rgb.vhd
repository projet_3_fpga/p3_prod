----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2021 09:53:00 AM
-- Design Name: 
-- Module Name: hsv2rgb - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hsv2rgb is
    port (
      hue : in STD_LOGIC_VECTOR (8 downto 0);
      sat : in STD_LOGIC_VECTOR (7 downto 0);
      val : in STD_LOGIC_VECTOR (7 downto 0);
      
      red : out STD_LOGIC_VECTOR (7 downto 0);
      green : out STD_LOGIC_VECTOR (7 downto 0);
      blue : out STD_LOGIC_VECTOR (7 downto 0);
      
      rgb_valid : out STD_LOGIC;

      clk : in STD_LOGIC;
      en : in STD_LOGIC
    );
end hsv2rgb;

architecture rtl of hsv2rgb is
  -- Déclaration des signaux
  type registered_t is record
    -- Cycle 1
    val_buf_1 : std_logic_vector (7 downto 0);
    v_times_s : std_logic_vector (15 downto 0);
    hue_adjust : std_logic_vector (6 downto 0);
    hue_msb_buf_1 : std_logic_vector(2 downto 0);

    -- Cycle 2
    val_buf_2 : std_logic_vector (7 downto 0);
    v_minus_vs : std_logic_vector (15 downto 0); -- unsigned
    vs_times_adj : std_logic_vector (22 downto 0);
    hue_msb_buf_2 : std_logic_vector(2 downto 0);

    -- Cycle 3
    red : std_logic_vector (7 downto 0);
    green : std_logic_vector (7 downto 0);
    blue : std_logic_vector (7 downto 0);
  end record registered_t;

  signal reg : registered_t;
  alias v_minus_vs_trunc : std_logic_vector (7 downto 0) is reg.v_minus_vs (15 downto 8);

begin

  pipeline: process(clk, en)
    -- Déclaration des variables
    -- Cycle 1
    variable hue_msbs : std_logic_vector (2 downto 0);
    variable hue_lsbs : std_logic_vector (5 downto 0);

    constant ONE : unsigned (6 downto 0) := "1000000";
    variable one_minus_lsbs : std_logic_vector(6 downto 0);

    -- Cycle 2
    
    -- Cycle 3
    variable v_minus_vs_times_adj : std_logic_vector (22 downto 0); -- point binaire fixe à 22 (un bit entier, le reste fractionnaire)
    -- Rogner le bit d'entier pour obtenir une fraction 8-bits
    alias v_minus_vs_times_adj_trunc : std_logic_vector (7 downto 0) is v_minus_vs_times_adj(21 downto 14);
  begin
    if rising_edge(clk) then
      if en = '1' then
        -- Cycle 1
        reg.val_buf_1 <= val;
        reg.v_times_s <= std_logic_vector(unsigned(val) * unsigned(sat));

        hue_msbs := hue(8 downto 6);
        hue_lsbs := hue(5 downto 0);
        
        one_minus_lsbs := std_logic_vector(ONE - unsigned('0' & hue_lsbs));

        reg.hue_adjust <= one_minus_lsbs when (hue_msbs = "001" or -- odd cases
                                               hue_msbs = "011" or
                                               hue_msbs = "101") else ('0' & hue_lsbs);
        reg.hue_msb_buf_1 <= hue_msbs;

        -- Cycle 2
        reg.val_buf_2 <= reg.val_buf_1;
        reg.v_minus_vs <= std_logic_vector(unsigned(reg.val_buf_1 & "00000000")
                                          - unsigned(reg.v_times_s));
        reg.vs_times_adj <= std_logic_vector(unsigned(reg.hue_adjust) * unsigned(reg.v_times_s));
        reg.hue_msb_buf_2 <= reg.hue_msb_buf_1;
        
        -- Cycle 3
        --PB dans la soustraction de deux non-signés
        v_minus_vs_times_adj := std_logic_vector(unsigned('0' & reg.val_buf_2 & "00000000000000") - unsigned(reg.vs_times_adj));

        -- Big multiplexers on RGB outputs
        case reg.hue_msb_buf_2 is
          when "000"  => reg.red <= reg.val_buf_2;
          when "001"  => reg.red <= reg.val_buf_2;
          when "010"  => reg.red <= v_minus_vs_times_adj_trunc;
          when "011"  => reg.red <= v_minus_vs_trunc;
          when "100"  => reg.red <= v_minus_vs_trunc;
          when others => reg.red <= v_minus_vs_times_adj_trunc;
        end case;

        case reg.hue_msb_buf_2 is
          when "000"  => reg.green <= v_minus_vs_trunc;
          when "001"  => reg.green <= v_minus_vs_times_adj_trunc;
          when "010"  => reg.green <= reg.val_buf_2;
          when "011"  => reg.green <= reg.val_buf_2;
          when "100"  => reg.green <= v_minus_vs_times_adj_trunc;
          when others => reg.green <= v_minus_vs_trunc;
        end case;

        case reg.hue_msb_buf_2 is
          when "000"  => reg.blue <= v_minus_vs_times_adj_trunc;
          when "001"  => reg.blue <= v_minus_vs_trunc;
          when "010"  => reg.blue <= v_minus_vs_trunc;
          when "011"  => reg.blue <= v_minus_vs_times_adj_trunc;
          when "100"  => reg.blue <= reg.val_buf_2;
          when others => reg.blue <= reg.val_buf_2;
        end case;

      else
        -- Reset les signaux registrés
        reg <= (
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),

          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),
          (others => '0'),

          (others => '0')
        );
      end if;
    end if;
  end process pipeline;
  
  red <= reg.red;
  green <= reg.green;
  blue <= reg.blue;

  data_valid_counter: process(clk, en)
    variable count : integer range 0 to 7;
  begin
    if rising_edge(clk) then
      if en = '1' then
        -- Increment counter until 3, then stop
        if (count < 3) then
          count := count + 1;
        else
          count := count;
        end if;
      else
        -- Reset counter
        count := 0;
      end if;
    end if;
    
    rgb_valid <= '1' when count = 3 else '0';
  end process data_valid_counter;
end rtl;
