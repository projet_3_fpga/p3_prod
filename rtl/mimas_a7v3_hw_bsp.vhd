----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/08/2021 02:39:57 PM
-- Design Name: 
-- Module Name: mimas_a7v3_hw_bsp - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mimas_a7v3_hw_bsp is
    port (
      btns_hw : in STD_LOGIC_VECTOR (3 downto 0);
      btns : out STD_LOGIC_VECTOR (3 downto 0);
      seven_segment_hw : out STD_LOGIC_VECTOR (7 downto 0);
      seven_segment : in STD_LOGIC_VECTOR (7 downto 0);
      seven_seg_ds_en_hw : out STD_LOGIC_VECTOR (3 downto 0);
      seven_seg_ds_en : in STD_LOGIC_VECTOR (3 downto 0);
      
      clk_100MHz : in STD_LOGIC -- used for debouncing only
    );
end mimas_a7v3_hw_bsp;

architecture rtl of mimas_a7v3_hw_bsp is
  -- Buttons
  signal btns_debounced : std_logic_vector(3 downto 0);
  signal btns_hw_n : std_logic_vector(3 downto 0);
  
  -- 7-segs
  signal seven_segment_n : std_logic_vector(7 downto 0); -- logic inverted, but scrambled segment order
  alias a is seven_segment_n(7);
  alias b is seven_segment_n(6);
  alias c is seven_segment_n(5);
  alias d is seven_segment_n(4);
  alias e is seven_segment_n(3);
  alias f is seven_segment_n(2);
  alias g is seven_segment_n(1);
  alias dp is seven_segment_n(0);

begin

  -- Debounce and flip button logic to normally-open
  btns_hw_n <= not btns_hw;
  debouncer_inst : entity work.grp_debouncer(rtl)
    generic map (
        N => 4, -- input bus width
        CNT_VAL => 10000 -- clock counts for debounce period
    )
    port map (
        clk_i => clk_100MHz, -- system clock
        data_i => btns_hw_n, -- noisy input data
        data_o => btns, -- registered stable output data
        strb_o => open -- strobe for new data available
    );

  -- Reorder segments ABCDEFG<DP> and flip logic to non-inverted
  seven_segment_hw(7) <= e;
  seven_segment_hw(6) <= d;
  seven_segment_hw(5) <= c;
  seven_segment_hw(4) <= dp;
  seven_segment_hw(3) <= b;
  seven_segment_hw(2) <= a;
  seven_segment_hw(1) <= f;
  seven_segment_hw(0) <= g;
  seven_segment_n <= not seven_segment;

  seven_seg_ds_en_hw <= not seven_seg_ds_en;
end rtl;
